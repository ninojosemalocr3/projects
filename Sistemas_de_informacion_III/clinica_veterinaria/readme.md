# Sistema de Gestión de Clínica Veterinaria

Bienvenido al Sistema de Gestión de Clínica Veterinaria HUELLAS. Este sistema es una herramienta diseñada para ayudar a las clínicas veterinarias a gestionar sus operaciones diarias de manera eficiente. Desde el seguimiento de pacientes y sus historiales médicos hasta la programación de citas y la gestión de facturación, nuestro sistema tiene como objetivo simplificar y optimizar las tareas relacionadas con la atención médica y el funcionamiento de la clínica.

## Integrantes GRUPO E
- Yihad Salek Villalba
- Edward Viruez Roca
- Dabbya Jaimes Martínez
- Antonio de Jesus Peredo
- Junior Aguilar Leaños

## Características Principales

- **Registro de Mascotas:** Mantenga un registro completo de todas las mascotas que visitan la clínica, incluidos sus datos, historial clínico y otra información relevante.

- **Historiales Médicos:** Registre el historial médico de cada mascota, incluyendo diagnósticos anteriores, tratamientos realizados, medicamentos recetados y resultados de pruebas.

- **Programación de Citas:** Programe citas para las mascotas de manera eficiente. Visualice el calendario de citas para planificar y organizar las consultas.

- **Inventario de Medicamentos:** Lleve un seguimiento de los medicamentos y suministros médicos disponibles en la clínica. Reciba alertas cuando los niveles de inventario estén bajos.

- **Pagos:** Registre los pagos de los clientes y realice un seguimiento de los estados de cuenta.


