# Proyecto formativo: Modelado Backend de un sistema de regitro de ventas de productos de limpieza para una empresa del rubro masivo
- Jorge Justiniano Parada
- [Facebook](https://www.facebook.com/jorge.justiniano.35)

# Introducción

El presente sistema se ha diseñado con el propósito de facilitar el registro y control de las ventas diarias de una empresa del rubro masivo, específicamente dedicada a la comercialización de productos de limpieza. El sistema permitirá a la empresa gestionar de manera eficiente las transacciones de venta, brindando información detallada y precisa sobre las operaciones realizadas.

# Objetivos

El objetivo principal del proyecto es desarrollar un sistema de registro de ventas que permita a la empresa JORGE SRL, ubicada en Santa Cruz de la Sierra, registrar y controlar de manera efectiva las ventas diarias de sus productos de limpieza. Para alcanzar este objetivo, se realizarán las siguientes tareas:

1. Análisis: Realizar un análisis detallado de los requerimientos del sistema y las funcionalidades necesarias para cubrir las necesidades de la empresa.
2. Modelado: Diseñar el modelo de base de datos que servirá como estructura para almacenar la información relacionada con las ventas, los productos, los clientes y otros elementos relevantes.
3. Migraciones: Implementar las migraciones necesarias para crear las tablas en la base de datos de acuerdo con el modelo diseñado.
4. Models: Crear los modelos de Eloquent correspondientes a cada tabla de la base de datos para interactuar con los datos de manera orientada a objetos.
5. Seeders: Generar datos de prueba utilizando seeders para poblar la base de datos con información inicial
6. API REST: Desarrollar una API RESTful que permita acceder y manipular los datos del sistema

# Marco Teórico

Para elaborar el proyecto hicimos los siguientes pasos:

1. Instalar Larago y luego composer
2. Entramos a la terminal de laragon.
3. Creamos un nuevo proyecto de laravel con el siguiente comando `composer create-project --prefer-dist laravel/laravel proyecto`
4. Genera una nueva clave de aplicación con `php artisan key:generate`.
5. Configura la conexión a la base de datos en el archivo `.env`.
6. Ejecuta las migraciones de la base de datos con `php artisan migrate`.
7. Inicia el servidor local con `php artisan serve`.
8. Abre tu navegador web y navega a la URL proporcionada por el servidor local para acceder a la aplicación.




# Metodología

En este documento se describe la metodología utilizada en el proyecto para llevar a cabo el desarrollo del sistema de registro de ventas de productos de limpieza para la empresa JORGE SRL.

## Análisis de Requerimientos
Identificamos los requisitos del sistema mediante entrevistas con los interesados y el análisis de la documentación proporcionada.
## Diseño del Modelo de Base de Datos
Diseñamos el modelo de base de datos utilizando un enfoque entidad-relación para representar las entidades, atributos y relaciones entre ellas.
## Creación de Migraciones
Implementamos las migraciones de Laravel para crear las tablas en la base de datos de acuerdo con el modelo diseñado.
## Desarrollo de Models
Creamos los modelos de Eloquent correspondientes a cada tabla de la base de datos para interactuar con los datos de manera orientada a objetos.
## Generación de Seeders
Generamos datos de prueba utilizando seeders para poblar la base de datos con información inicial.
## Desarrollo de la API REST
Implementamos una API RESTful utilizando Laravel para permitir el acceso y la manipulación de los datos del sistema de forma remota.
## Pruebas y Depuración
Realizamos pruebas exhaustivas del sistema para garantizar su funcionamiento correcto y corregir cualquier error o fallo identificado.
## Despliegue y Puesta en Marcha
Desplegamos la aplicación en un entorno de producción y la pusimos en marcha para su uso por parte de los usuarios finales.


#	Modelado o Sistematización

![diagrama](https://live.staticflickr.com/65535/53558793168_2c6b276d8f_b.jpg)

### Migracion

```bash


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('categorias', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->decimal('peso', 8, 2);
            $table->decimal('precio', 8, 2);
            $table->foreignId('id_categoria')->constrained('categorias');
            $table->softDeletes();
            $table->timestamps();
        });

     
        Schema::create('zonas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('rutas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('referencia');
            $table->foreignId('id_zona')->constrained('zonas');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('email');
            $table->foreignId('id_ruta')->constrained('rutas');
            $table->string('direccion');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('vendedores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->softDeletes();
            $table->timestamps();
        });

       

        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_vendedor')->constrained('vendedores');
            $table->foreignId('id_cliente')->constrained('clientes');
            $table->decimal('total', 10, 2);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('venta_detallada', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_venta')->constrained('ventas');
            $table->foreignId('id_producto')->constrained('productos');
            $table->foreignId('id_vendedor')->constrained('vendedores');
            $table->foreignId('id_cliente')->constrained('clientes');
            $table->integer('cantidad');
            $table->decimal('total', 10, 2);
            $table->softDeletes();
            $table->timestamps();
        });
       

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('venta_detallada');
        Schema::dropIfExists('ventas');
        Schema::dropIfExists('vendedores');
        Schema::dropIfExists('rutas');
        Schema::dropIfExists('zonas');
        Schema::dropIfExists('clientes');
        Schema::dropIfExists('productos');
        Schema::dropIfExists('categorias');
    }
}

```

### Api

```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Producto;
use App\Models\Vendedor;
use App\Models\VentaDetallada;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});




// 1. Total vendido por cada vendedor, ordenado de mayor a menor, con el nombre del vendedor
Route::get('total-vendido-por-vendedor', function() {
    return Vendedor::select('nombre', \DB::raw('SUM(ventas.total) as total_vendido'))
        ->join('ventas', 'vendedores.id', '=', 'ventas.id_vendedor')
        ->groupBy('vendedores.id', 'nombre')
        ->orderByDesc('total_vendido')
        ->get();
});

// 2. Total vendido bs por producto, ordenado de mayor a menor, con el nombre del producto
Route::get('total-vendido-por-producto', function() {
    return Producto::select('nombre', \DB::raw('SUM(venta_detallada.total) as total_vendido'))
        ->join('venta_detallada', 'productos.id', '=', 'venta_detallada.id_producto')
        ->groupBy('productos.id', 'nombre')
        ->orderByDesc('total_vendido')
        ->get();
});

// 3. Cantidad total de los productos vendidos ordenado de mayor a menor
Route::get('cantidad-total-productos-vendidos', function() {
    return VentaDetallada::select('id_producto', 'nombre', \DB::raw('SUM(cantidad) as cantidad_total_vendida'))
        ->join('productos', 'venta_detallada.id_producto', '=', 'productos.id')
        ->groupBy('id_producto', 'nombre')
        ->orderByDesc('cantidad_total_vendida')
        ->get();
});
// 4. ruta que mas vende en bs
Route::get('ruta-mas-vendida', function() {
    return \DB::table('rutas')
        ->select('rutas.nombre', \DB::raw('SUM(ventas.total) as total_vendido'))
        ->join('clientes', 'rutas.id', '=', 'clientes.id_ruta')
        ->join('ventas', 'clientes.id', '=', 'ventas.id_cliente')
        ->groupBy('rutas.id', 'rutas.nombre')
        ->orderByDesc('total_vendido')
        ->get();
});
// 5. ruta que mas vende en bs
Route::get('zona-mas-vendida', function() {
    return \DB::table('zonas')
        ->select('zonas.nombre', \DB::raw('SUM(ventas.total) as total_vendido'))
        ->join('rutas', 'zonas.id', '=', 'rutas.id_zona')
        ->join('clientes', 'rutas.id', '=', 'clientes.id_ruta')
        ->join('ventas', 'clientes.id', '=', 'ventas.id_cliente')
        ->groupBy('zonas.id', 'zonas.nombre')
        ->orderByDesc('total_vendido')
        ->get();
});
//6.- total por categorias
Route::get('total-por-categorias', function() {
    return \DB::table('categorias')
        ->select('categorias.nombre', \DB::raw('SUM(venta_detallada.total) as total_vendido'))
        ->join('productos', 'categorias.id', '=', 'productos.id_categoria')
        ->join('venta_detallada', 'productos.id', '=', 'venta_detallada.id_producto')
        ->groupBy('categorias.id', 'categorias.nombre')
        ->orderByDesc('total_vendido')
        ->get();
});
//7.- total por cliente
Route::get('clientes-con-total-vendido', function() {
    return \DB::table('clientes')
        ->select('clientes.id', 'clientes.nombre', 'clientes.apellido', \DB::raw('SUM(ventas.total) as total_vendido'))
        ->join('ventas', 'clientes.id', '=', 'ventas.id_cliente')
        ->groupBy('clientes.id', 'clientes.nombre', 'clientes.apellido')
        ->orderByDesc('total_vendido')
        ->get();
});

//8.- cantidad de clientes por ruta
Route::get('cantidad-clientes-por-ruta', function() {
    return \DB::table('rutas')
        ->select('rutas.nombre', \DB::raw('COUNT(clientes.id) as cantidad_clientes'))
        ->leftJoin('clientes', 'rutas.id', '=', 'clientes.id_ruta')
        ->groupBy('rutas.id', 'rutas.nombre')
        ->orderByDesc('cantidad_clientes')
        ->get();
});

//9.- cantidad de clientes por zona
Route::get('cantidad-clientes-por-zona', function() {
    return \DB::table('zonas')
        ->select('zonas.nombre', \DB::raw('COUNT(clientes.id) as cantidad_clientes'))
        ->leftJoin('rutas', 'zonas.id', '=', 'rutas.id_zona')
        ->leftJoin('clientes', 'rutas.id', '=', 'clientes.id_ruta')
        ->groupBy('zonas.id', 'zonas.nombre')
        ->orderByDesc('cantidad_clientes')
        ->get();
});

//10.- listado de rutas por zona y cantidad de clientes
Route::get('zonas-con-rutas-y-clientes', function() {
    return \DB::table('zonas')
        ->select('zonas.nombre as zona', 'rutas.nombre as ruta', \DB::raw('COUNT(clientes.id) as cantidad_clientes'))
        ->leftJoin('rutas', 'zonas.id', '=', 'rutas.id_zona')
        ->leftJoin('clientes', 'rutas.id', '=', 'clientes.id_ruta')
        ->groupBy('zonas.id', 'zonas.nombre', 'rutas.id', 'rutas.nombre')
        ->orderBy('zonas.nombre')
        ->orderBy('rutas.nombre')
        ->get();
});








```

##	Conclusiones
En conclusión, el desarrollo de este sistema de registro de ventas de productos de limpieza para la empresa JORGE SRL ha sido un paso fundamental hacia la mejora de la gestión y el control de las operaciones diarias. Con la implementación exitosa de una base de datos estructurada, una API REST funcional y un proceso de prueba exhaustivo, el sistema proporciona una plataforma sólida para registrar, gestionar y analizar las ventas de la empresa de manera eficiente. Este proyecto demuestra el compromiso de JORGE SRL con la innovación y la mejora continua, permitiendo un mejor servicio a los clientes y un mayor crecimiento empresarial en el competitivo mercado de productos de limpieza
##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234



## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
