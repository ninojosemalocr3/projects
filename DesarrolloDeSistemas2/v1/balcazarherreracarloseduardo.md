# Proyecto Formativo: Modelado Backend de un sistema de ventas de postres.
- Carlos Eduardo Balcazar Herrera
- [Whatsapp]( https://wa.me/76096858)

##	Introducción
Este sistema esta desarrollado para la empresa que KeDulce que se dedica a la elaboracion y venta de todo tipo de postres como ser: tortas, brownie, galletas bañadas en chocolate entre muchos postres mas.

##	Objetivos
Dearrollar el Analisis, Modelado, Migraciones, Models, Seeder y la Api Rest para la empresa KeDulce que se encuentra en la ciudad de Santa Cruz de la Sierra. 

##	Marco Teórico
###	Laravel

Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:
```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```

3. **Configurar el archivo .env.example**: Dejalo como .env y dentro colocar todas las variables de entorno de nuestro proyecto.

4. **Creamos la base de datos para nuestro proyecto**: Con el nombre que sea conveniente.

5. **Generar una APP_KEY**: Que es una llave para cada proyecto de Laravel se puede generar con este comando:
```bash
php artisan key:generate
```
5. **Generar las migraciones**: Ejecutar los seeders para nuestras tablas de base de datos con este comando:
```bash
php artisan migrate --seed
```

### Swagger
### Instalacion & Configuracion
Sigue estos pasos para instalar Swagger en tu sistema:
- instala el Swagger 
```bash
 	composer require "darkaonline/l5-swagger"
```
- abra su config/app.php y agregue esta línea en la sección de proveedores
```bash
 	L5Swagger\L5SwaggerServiceProvider::class,
```
- Puede publicar la configuración de L5-Swagger y ver archivos en su proyecto ejecutando:
```bash
 	php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
    php artisan l5-swagger:generate
```

##	Metodología
### Relevamiento de Requisitos:
- Entender las necesidades específicas de KeDulce.
- Identificar los tipos de postres, procesos de venta y gestión que requieren.
### Implementación de Funcionalidades Clave:
- Desarrollar módulos para la creación, edición y eliminación de productos.
- Implementar procesos de venta y control de inventario.
### Seguridad y Validación:
- Aplicar medidas de seguridad como incriptar la contraseñas de los usuarios.
- Garantizar el manejo seguro de datos.
##	Modelado o Sistematización
### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53558549956_6fdc6829a9_c.jpg)

### Diagrama entidad relacion.
### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('categorias', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('inventarios', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad_disponible');
            $table->date('fecha_vencimiento');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categoria_id');
            $table->unsignedBigInteger('inventario_id');
            $table->string('nombre');
            $table->decimal('precio', 10, 2);
            $table->string('descripcion');
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('inventario_id')->references('id')->on('inventarios');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('detalle_ventas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('venta_id');
            $table->unsignedBigInteger('producto_id');
            $table->integer('cantidad_vendida');
            $table->decimal('precio_unitario', 10, 2);
            $table->decimal('subtotal', 10, 2);
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_ventas');
        Schema::dropIfExists('productos');
        Schema::dropIfExists('inventarios');
        Schema::dropIfExists('categorias');
        Schema::dropIfExists('ventas');
    }
}

```

### Datos seeder
### DatabaseSeeder.php
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UsersSeeder::class);
        $this->call(GeneralSeeder::class);
    }
}

```
### GeneralSeeder.php
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Venta;
use App\Models\Categoria;
use App\Models\Producto;
use App\Models\Inventario;
use App\Models\Detalle_Venta;


class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Categoria::create(['nombre'=>'Pastel']);
        Categoria::create(['nombre'=>'Dulces']);
        Categoria::create(['nombre'=>'Jugos']);
        Categoria::create(['nombre'=>'Saladitos']);

        Inventario::create(['cantidad_disponible'=>'5','fecha_vencimiento'=>'2024-03-06']);
        Inventario::create(['cantidad_disponible'=>'10','fecha_vencimiento'=>'2024-03-06']);
        Inventario::create(['cantidad_disponible'=>'15','fecha_vencimiento'=>'2024-03-10']);
        Inventario::create(['cantidad_disponible'=>'30','fecha_vencimiento'=>'2024-03-04']);

        
        Venta::create(['fecha'=>now(),'total'=>'100','user_id'=>1,]);
        Venta::create(['fecha'=>now(),'total'=>'60','user_id'=>1,]);
        Venta::create(['fecha'=>now(),'total'=>'40','user_id'=>2,]);
        Venta::create(['fecha'=>now(),'total'=>'100','user_id'=>4,]);


        Producto::create(['nombre'=>'Torta De Chocolate','precio'=>'100',
        'descripcion'=>'Torta para 30 personas con chocolate y rellenos de crema','categoria_id'=>1,
        'inventario_id'=>1,]);
        Producto::create(['nombre'=>'Brownie','precio'=>'30',
        'descripcion'=>'Brownie de chocolate','categoria_id'=>1,'inventario_id'=>1,]);
        Producto::create(['nombre'=>'Manzana','precio'=>'20',
        'descripcion'=>'Manzanas bañadas con dulce de fresa','categoria_id'=>2,'inventario_id'=>2,]);
        Producto::create(['nombre'=>'Bolitas de carne','precio'=>'10',
        'descripcion'=>'Bolitas de carne en forma redonda','categoria_id'=>4,'inventario_id'=>3,]);

        Detalle_Venta::create(['cantidad_vendida'=>'5','precio_unitario'=>'100',
        'subtotal'=>'100','venta_id'=>1,'producto_id'=>1,]);
        Detalle_Venta::create(['cantidad_vendida'=>'2','precio_unitario'=>'30',
        'subtotal'=>'60','venta_id'=>1,'producto_id'=>1,]);
        Detalle_Venta::create(['cantidad_vendida'=>'2','precio_unitario'=>'20',
        'subtotal'=>'40','venta_id'=>2,'producto_id'=>2,]);
        Detalle_Venta::create(['cantidad_vendida'=>'10','precio_unitario'=>'10',
        'subtotal'=>'100','venta_id'=>4,'producto_id'=>3,]);

    }
}
```
### UsersSeeder.php
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory as Faker;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,5) as $i){
            User::create([
                'name'=>$faker->name,
                'paterno'=>$faker->lastName,
                'materno'=>$faker->lastName,
                'email'=>$faker->unique()->safeEmail,
                'password'=>bcrypt('1234567'),
            ]);
        }
        
        
    }
}

```

### APi.
```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Ejemplo de routes/api.php

use App\Http\Controllers\ProductoController;
use App\Http\Controllers\InventarioController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\VentaController;
use App\Http\Controllers\UserController;

use App\Models\Inventario;
use App\Models\User;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Venta;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('productos', ProductoController::class);
Route::resource('inventarios', InventarioController::class);
Route::resource('categorias', CategoriaController::class);
Route::resource('ventas', VentaController::class);
Route::resource('users', UserController::class);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ejemplo',function(){
    return Inventario::all();
});

Route::get('usuarios',function(){
    return User::all();
});

Route::get('inventarios',function(){
    return Inventario::all();
});
Route::get('productos',function(){
    return Producto::all();
});

Route::get('categorias',function(){
    return Categoria::all();
});

Route::get('ventas',function(){
    return Venta::all();
});

Route::get('users',function(){
    return User::all();
});
```

##	Conclusiones
Este es un sistema la cual tiene como enfoque tecnológico no solo agilizar los procesos internos, sino que también mejorar la experiencia del cliente al facilitar el acceso a información actualizada. KeDulce está preparada para prosperar en la dinámica industria de la repostería en Santa Cruz de la Sierra.

### Posibles áreas de mejora.
Implementación de Pago en Línea (Opcional):

- Integrar pasarelas de pago para pedidos en línea.

##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234

- Documenta tu API con Swagger en Laravel 🎁 - Document your API with Swagger in Laravel 🏆,  https://www.youtube.com/watch?v=kUCT88EJuCU
##	Anexos
### Logo
![logo](https://live.staticflickr.com/65535/53557743862_40c3c8575c_c.jpg)


