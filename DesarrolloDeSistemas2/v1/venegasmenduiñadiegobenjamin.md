# proyecto formativo: Modelado Backend de un sistema de Gestion Tienda Jired.
- Diego Benjamin Venegas Menduiña
- [Facebook](https://www.facebook.com/zambranachaconjaime/)

##	Introducción
este sistema esta creado para poder afministrar una Tienda de Carpas, que tiene los siguientes requerimientos, poder registrar sus clientes, las ventas que efectuan, el inventario de 
las carpas por medida, color, calidad, gramaje, tambien se requiere poder registrar a los trabajadores y sus horarios y tambien poder guardar informacion de los proveedores.

##	Objetivos
Objetivo General:

Desarrollar un sistema de gestión integral para una tienda de carpas que permita administrar eficazmente las ventas, clientes, inventario, recursos humanos y proveedores, con el fin de optimizar sus operaciones y mejorar la experiencia del cliente.

Objetivos Específicos:

Diseñar y desarrollar una base de datos relacional que permita almacenar y gestionar de manera eficiente la información relacionada con clientes, ventas, inventario, empleados, sueldos, facturas y proveedores.

Implementar una interfaz de usuario amigable e intuitiva que permita a los usuarios realizar tareas como registrar ventas, administrar inventario, gestionar empleados y proveedores, generar informes y realizar consultas de manera sencilla.

Facilitar el seguimiento y análisis de las ventas, mediante la generación de informes y estadísticas que proporcionen insights sobre el rendimiento del negocio, las tendencias de ventas y el comportamiento del cliente.

Mejorar la eficiencia en la gestión de inventario, asegurando un control preciso de las existencias, la reposición oportuna de productos y la identificación de productos de bajo rendimiento.

Agilizar los procesos de recursos humanos, incluyendo el registro de horarios de empleados, el cálculo de sueldos y la generación de informes relacionados con la gestión del personal.

Fortalecer las relaciones con proveedores, facilitando la gestión de pedidos, la recepción de productos y la comunicación con los proveedores de manera efectiva.

Al cumplir con estos objetivos, se espera que el sistema contribuya significativamente a la eficiencia operativa y el éxito comercial de la tienda de carpas.

##	Marco Teórico
###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).
```bash
composer install
```

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project laravel/laravel ApiRest
```
3. **Crear un nuevo modelo**: Utiliza php artizan  para crear un modelo llamado "Customer" El flag --all crea también las migraciones, el factory y el seeder asociados al modelo.
```bash
php artisan make:model Customer --all
```
4. **Ejecutar Migraciones**: Una vez que has creado tu modelo y definido la estructura de tu base de datos, necesitas ejecutar las migraciones para crear las tablas correspondientes en la base de datos. Utiliza el siguiente comando:

```bash
php artisan migrate
```
5. **Sembrar la Base de Datos (Opcional)**: Si deseas poblar tu base de datos con datos de prueba, puedes utilizar seeders. Esto es útil para tener datos de muestra con los que trabajar durante el desarrollo. Ejecuta el siguiente comando para ejecutar los seeders:
```bash
php artisan db:seed
```
6. **Crear un Recurso de Colección**: Para manejar la transformación de una colección de modelos de clientes antes de devolverlos como respuestas JSON, vamos a crear un recurso de colección llamado "CustomerCollection". Esto nos permitirá estructurar y personalizar la salida de la API para las colecciones de recursos. Ejecuta el siguiente comando:
```bash
php artisan make:resource CustomerCollection
```
7. **Acceder a la API Local**: Una vez que tu proyecto está en funcionamiento, puedes acceder a la API localmente utilizando la dirección proporcionada. Si estás siguiendo las convenciones típicas de Laravel, la dirección de tu API sería algo así como http://127.0.0.1:8000/api/v1/customers. Asegúrate de reemplazar "8000" con el puerto en el que está ejecutando tu servidor local, si es diferente.
```bash
php artisan serve
```

###	MVC
###	Swagger para laravel

1. **Instala Swagger en tu proyecto Laravel**: Para instalar Swagger en tu proyecto Laravel, puedes usar el paquete `darkaonline/l5-swagger`. Abre una terminal en la raíz de tu proyecto Laravel y ejecuta el siguiente comando: composer require darkaonline/l5-swagger

2. **Publica los archivos de configuración de Swagger**: Después de instalar el paquete, debes publicar los archivos de configuración de Swagger. Ejecuta el siguiente comando en la terminal de tu proyecto Laravel:

php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"


3. **Configura Swagger**: Abre el archivo de configuración de Swagger en `config/l5-swagger.php` y ajusta la configuración según tus necesidades. Puedes configurar cosas como la ruta de la documentación Swagger, el título, la descripción, etc.

4. **Genera la documentación Swagger**: Una vez que hayas configurado todo, puedes generar la documentación Swagger ejecutando el siguiente comando:


5. **Accede a la documentación Swagger**: Una vez que hayas generado la documentación, puedes acceder a ella a través del navegador. Por lo general, la URL será `http://localhost/mi-proyecto/api/documentation`. Asegúrate de reemplazar `mi-proyecto` con el nombre de tu proyecto Laravel.



..
...

##	Metodología


### 1. Planificación y Análisis:
   - Identificar y comprender los requisitos del sistema a través de reuniones con los interesados y el equipo de desarrollo.
   - Definir los objetivos específicos del proyecto y los requisitos funcionales y no funcionales del sistema.
   - Realizar un análisis detallado de los procesos de negocio de la tienda y las entidades involucradas.

### 2. Diseño de la Base de Datos:
   - Diseñar el modelo conceptual de la base de datos, identificando las entidades, atributos y relaciones principales.
   - Refinar el modelo conceptual y convertirlo en un modelo lógico, utilizando herramientas como diagramas entidad-relación (ER).
   - Normalizar el modelo lógico para eliminar redundancias y mejorar la integridad de los datos.

### 3. Configuración del Entorno de Desarrollo:
   - Instalar y configurar Laragon como entorno de desarrollo local, que incluye  y otras herramientas necesarias.
   - Configurar Laravel, un marco de trabajo de PHP, para facilitar el desarrollo de la aplicación web.
   - Configurar HeidiSQL como cliente de base de datos para interactuar con la base de datos MySQL.





##	Modelado o Sistematización
### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53559024890_e74f524ae1_z.jpg)
### Diagrama entidad relacion.
![diagrama](https://live.staticflickr.com/65535/53558600671_268d40ab1d.jpg)
### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Tabla clientes
        Schema::create('clientes', function (Blueprint $table) {
           $table->id();
           $table->string('nombre')->nullable();
           $table->string('apellido')->nullable();
           $table->string('direccion')->nullable();
           $table->string('telefono')->nullable();
           $table->string('correo')->unique();
           $table->timestamps();
           $table->softDeletes();

       });

       // Tabla carpas
       Schema::create('carpas', function (Blueprint $table) {
           $table->id();
           $table->string('modelo')->nullable();
           $table->string('tamano')->nullable();
           $table->string('material')->nullable();
           $table->decimal('precio', 8, 2)->nullable();
           $table->decimal('precio_compra', 8, 2)->nullable();

           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla proveedores
       Schema::create('proveedores', function (Blueprint $table) {
           $table->id();
           $table->string('nombre')->nullable();
           $table->string('contacto')->nullable();
           $table->string('telefono')->nullable();
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla ventas
       Schema::create('ventas', function (Blueprint $table) {
           $table->id();
           $table->foreignId('cliente_id')->references('id')->on('clientes');
           $table->date('fecha')->nullable();
           $table->decimal('total', 8, 2)->nullable();
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla inventarios
       Schema::create('inventarios', function (Blueprint $table) {
        $table->id();
        $table->foreignId('carpa_id')->constrained('carpas');
        $table->integer('cantidad')->default(0); // Cantidad inicial en 0
        $table->timestamps();
        $table->softDeletes();
    });

       // Tabla detalle_ventas
       Schema::create('detalle_ventas', function (Blueprint $table) {
           $table->id();
           $table->foreignId('venta_id')->references('id')->on('ventas');
           $table->foreignId('carpa_id')->references('id')->on('carpas');
           $table->integer('cantidad')->nullable();
           $table->decimal('precio_unitario', 8, 2)->nullable();
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla facturas
       Schema::create('facturas', function (Blueprint $table) {
           $table->id();
           $table->foreignId('venta_id')->references('id')->on('ventas');
           $table->decimal('total', 8, 2)->nullable();
           $table->date('fecha_emision')->nullable();
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla empleados
       Schema::create('empleados', function (Blueprint $table) {
           $table->id();
           $table->string('nombre')->nullable();
           $table->string('apellido')->nullable();
           $table->string('puesto')->nullable();
           $table->date('fecha_contratacion')->nullable();
           $table->decimal('salario', 8, 2)->nullable();
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla usuarios
       Schema::create('usuarios', function (Blueprint $table) {
           $table->id();
           $table->string('nombre_usuario')->unique();
           $table->string('contrasena');
           $table->enum('nivel_acceso', ['admin', 'empleado'])->default('empleado');
           $table->foreignId('empleado_id')->nullable()->constrained('empleados');
           $table->timestamps();
           $table->softDeletes();
       });

       // Tabla horarios
       Schema::create('horarios', function (Blueprint $table) {
           $table->id();
           $table->string('dia_semana')->nullable();
           $table->time('hora_inicio')->nullable();
           $table->time('hora_fin')->nullable();
           $table->timestamps();
           $table->softDeletes();
       });
   }

   /**
    * Reverse the migrations.
    */
   public function down(): void
   {

    Schema::table('usuarios', function (Blueprint $table) {
        $table->dropForeign(['empleado_id']);
    });
       Schema::dropIfExists('detalle_ventas');
       Schema::dropIfExists('facturas');
       Schema::dropIfExists('empleados');
       Schema::dropIfExists('usuarios');
       Schema::dropIfExists('horarios');
       Schema::dropIfExists('ventas'); // Elimina la tabla ventas después de las tablas que tienen una relación de clave externa
       Schema::dropIfExists('inventarios'); // Elimina la tabla inventarios después de la tabla ventas para evitar problemas con la restricción de clave externa
       Schema::dropIfExists('carpas'); // Elimina la tabla carpas después de la tabla inventarios
       Schema::dropIfExists('clientes'); // Elimina la tabla clientes después de la tabla ventas para evitar problemas con la restricción de clave externa
       Schema::dropIfExists('proveedores');
   }
};


```
### explicacion del un caso de modelado.

### Datos seeder
### APi.

##	Conclusiones
Se presentan las conclusiones del proyecto, resumiendo los hallazgos más importantes, destacando los logros alcanzados y discutiendo posibles áreas de mejora o futuras investigaciones.
##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234
##	Anexos
### foto de la empresa para la cual estan desarrollando
### ubicacion
