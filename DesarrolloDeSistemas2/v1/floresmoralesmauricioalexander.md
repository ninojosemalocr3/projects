# proyecto formativo: Modelado Backend de un sistema de gestion de Reservas de Habitaciones.
- Mauricio Alexander Flores Morales
- [Facebook](https://www.facebook.com/mauricioalexander.floresmorales.9/)


## Introduccion
Este sistema esta creado para facilitar el proceso de reservas de habitaciones de Hoteles y ofrecer una experiencia óptima a los  huéspedes.
Este proyecto se enfoca en el modelado backend de un sistema que permita a hoteles y otros alojamientos a gestionar de manera eficiente sus reservas, garantizando una experiencia fluida tanto para los clientes como para el personal de administración.

## Objetivos
Desarrollar el análisis, modelado, migraciones, modelos, seeders y la API REST para la gestión integral de reservas de habitaciones del Hotel Cortez en la ciudad de Santa Cruz de la Sierra.

## Marco Teorico
### Laravel
Laravel es un popular framework de desarrollo de aplicaciones web PHP que sigue el patrón de arquitectura MVC (Modelo-Vista-Controlador). Ofrece una estructura de código limpia y elegante.

Instalación de Laravel
Para instalar Laravel en tu sistema, sigue estos pasos:

1. **Instalar Composer:** Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde getcomposer.org.

2. **Crear un Nuevo Proyecto de Laravel:** Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```
![Logo](https://live.staticflickr.com/65535/53570242240_7c51223044_m.jpg)

### Git y GitHub
Para el control de versiones de tu código fuente y la colaboración con otros desarrolladores en el proyecto.

![Logo](https://live.staticflickr.com/65535/53570137084_54dc24dc90.jpg)

### HeidiSQL 
Es una herramienta de administración y exploración de bases de datos relacionales que se utiliza principalmente para trabajar con servidores de bases de datos MySQL, MariaDB, PostgreSQL y Microsoft SQL Server. Permite a los usuarios conectarse a múltiples servidores de bases de datos simultáneamente, explorar las estructuras de las bases de datos.

![Logo](https://live.staticflickr.com/65535/53569810666_15264c5bde.jpg)

### Swagger 
Swagger es una herramienta que permite definir la estructura de una API web utilizando un lenguaje descriptivo como YAML o JSON, lo que facilita la comprensión de los endpoints, los parámetros necesarios, los tipos de respuesta esperados y otros detalles relevantes.

![Logo](https://live.staticflickr.com/65535/53568956547_caa6afbe60.jpg)

## Metodologia
Para documentar la metodología del proyecto, se siguió un enfoque ágil estructurado que incluyó las siguientes etapas:

1. **Definición de Objetivos:** 
Se establecieron los objetivos del proyecto, incluyendo sus metas principales y los resultados esperados.

2. **Análisis de Requerimientos:** Se recopilaron y analizaron los requisitos del proyecto, identificando las necesidades del usuario, los casos de uso principales y las funcionalidades clave que se debían implementar.

3. **Planificación y Diseño:** Se elaboró un plan detallado para el desarrollo del proyecto, que incluyó la planificación de tareas, la asignación de recursos y la definición de los plazos de entrega. Además, se diseñó la arquitectura del sistema y se crearon diagramas de flujo y modelos de datos para guiar la implementación.

4. **Implementación:** Se llevó a cabo la implementación del proyecto siguiendo las mejores prácticas de desarrollo de software. Se utilizaron tecnologías y herramientas adecuadas para cada tarea, y se realizó un seguimiento regular del progreso para garantizar el cumplimiento de los objetivos establecidos.

5. **Pruebas y Validación:** Se realizaron pruebas exhaustivas para validar el funcionamiento del proyecto y asegurar su calidad. Se llevaron a cabo pruebas unitarias, de integración y de aceptación del usuario para identificar y corregir posibles errores o problemas.

6. **Documentación:** Se elaboró una documentación completa del proyecto, que incluyó manuales de usuario, guías de instalación y cualquier otra información relevante para facilitar su comprensión y uso.

## Modelado o Sistematizacion
### Diagrama de clases 
![diagrama](https://live.staticflickr.com/65535/53570061374_4e9150014b_b.jpg)

### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        
        
        //
        Schema::create('habitacion', function (Blueprint $table) {
            $table->id();
            $table->integer('numero');
            $table->string('tipo');
            $table->double('precioNoche');
            $table->boolean('disponibilidad');
            $table->integer('capacidad');
            $table->timestamps();
            $table->softDeletes();
        });

            // Tabla Cliente
            Schema::create('cliente', function (Blueprint $table) {
                $table->id();
                $table->string('nombre');
                $table->string('apellido');
                $table->string('email')->unique();
                $table->string('telefono');
                $table->timestamps();
                $table->softDeletes();
            });
             // Tabla FormaPago
        Schema::create('forma_pago', function (Blueprint $table) {
            $table->id();
            $table->string('metodo');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });
        
        // Tabla Reserva
        Schema::create('reserva', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('habitacionId');
            $table->unsignedBigInteger('clienteId');
            $table->unsignedBigInteger('forma_pago_id')->nullable();
            
            $table->date('fechaEntrada');
            $table->date('fechaSalida');
            $table->foreign('habitacionId')->references('id')->on('habitacion');
            $table->foreign('clienteId')->references('id')->on('cliente');
            $table->foreign('forma_pago_id')->references('id')->on('forma_pago');
            $table->string('estado')->default('Pendiente');
        
            $table->timestamps();
            $table->softDeletes();
        });
        
    
        
        // Tabla Empleado
        Schema::create('empleado', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('cargo');
            $table->float('salario');
            $table->date('fechaIngreso');
            $table->timestamps();
            $table->softDeletes();
        });
        
       
        
        // Tabla Opinión
        Schema::create('opinion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('clienteId');
            $table->unsignedBigInteger('habitacionId');
            $table->string('comentario');
            $table->integer('calificacion');
            $table->foreign('clienteId')->references('id')->on('cliente');
            $table->foreign('habitacionId')->references('id')->on('habitacion'); 
            $table->timestamps();
            $table->softDeletes();
        });
        
        // Tabla ServicioHotel
        Schema::create('servicio', function (Blueprint $table) {
            $table->id();
            $table->string('tipo');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });
        
        // Tabla HabitacionServicioHotel
        Schema::create('habitacion_servicio', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('habitacionId');
            $table->unsignedBigInteger('servicioHotelId');
            $table->foreign('habitacionId')->references('id')->on('habitacion');
            $table->foreign('servicioHotelId')->references('id')->on('servicio');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('habitacion_servicio', function (Blueprint $table) {
            // Verificar si la clave externa existe antes de intentar eliminarla
            if (Schema::hasColumn('habitacion_servicio', 'habitacionId')) {
                $table->dropForeign(['habitacionId']);
            }
            if (Schema::hasColumn('habitacion_servicio', 'servicioHotelId')) {
                $table->dropForeign(['servicioHotelId']);
            }
        });
    

        //
        Schema::dropIfExists('reserva');
        Schema::dropIfExists('opinion');
        Schema::dropIfExists('forma_pago');
        Schema::dropIfExists('habitacion');
        
        Schema::dropIfExists('cliente');
        Schema::dropIfExists('empleado');
        
        Schema::dropIfExists('servicio');
        Schema::dropIfExists('habitacion_servicio');




    }
};

```

### Modelos
## Models/Cliente

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['nombre', 'apellido', 'email', 'telefono'];

    protected $table = 'cliente'; 

    public function reservas()
    {
        return $this->hasMany(Reserva::class, 'clienteId'); 
    }
}

```
## Models/Habitacion
```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Habitacion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['numero', 'tipo', 'precioNoche', 'disponibilidad', 'capacidad']; 

    protected $table = 'habitacion'; 

   
    public function reservas()
    {
        return $this->hasMany(Reserva::class, 'habitacionId'); 
    }

   
    public function servicios()
    {
        return $this->belongsToMany(Servicio::class, 'habitacion_servicio', 'habitacionId', 'servicioHotelId');
    }
}

```
### Seeder
## HabitacionSeeder
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Habitacion;

class HabitacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $habitacion = [

        [
            'numero' => '101',
            'tipo' => 'Suite',
            'precioNoche' => 250,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '102',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '103',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '104',
            'tipo' => 'Individual',
            'precioNoche' => 100,
            'disponibilidad' => true,
            'capacidad' => 1,
        ],
        [
            'numero' => '105',
            'tipo' => 'Suite',
            'precioNoche' => 250,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '106',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '107',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '108',
            'tipo' => 'Individual',
            'precioNoche' => 100,
            'disponibilidad' => true,
            'capacidad' => 1,
        ],
        [
            'numero' => '109',
            'tipo' => 'Suite',
            'precioNoche' => 250,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '110',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '111',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '112',
            'tipo' => 'Individual',
            'precioNoche' => 100,
            'disponibilidad' => false,
            'capacidad' => 1,
        ],
        [
            'numero' => '113',
            'tipo' => 'Suite',
            'precioNoche' => 250,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '114',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '115',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => false,
            'capacidad' => 2,
        ],
        [
            'numero' => '116',
            'tipo' => 'Individual',
            'precioNoche' => 100,
            'disponibilidad' => true,
            'capacidad' => 1,
        ],
        [
            'numero' => '117',
            'tipo' => 'Suite',
            'precioNoche' => 250,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '118',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '119',
            'tipo' => 'Doble',
            'precioNoche' => 150,
            'disponibilidad' => true,
            'capacidad' => 2,
        ],
        [
            'numero' => '120',
            'tipo' => 'Individual',
            'precioNoche' => 100,
            'disponibilidad' => true,
            'capacidad' => 1,
        ],
    ];
    Habitacion::insert($habitacion);
        
    }
}

```

### APi
## Habitacion
```bash
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Habitacion;



class HabitacionController extends Controller
{
    /**
 * Mostrar Lista de Habitaciones
 * @OA\Get(
 *     path="/api/habitaciones",
 *     tags={"Habitacion"},
 *     @OA\Response(
 *         response=200,
 *         description="OK",
 *         @OA\JsonContent(
 *             @OA\Property(
 *                 type="array",
 *                 property="rows",
 *                 @OA\Items(
 *                     type="object",
 *                     @OA\Property(
 *                         property="id",
 *                         type="number",
 *                         example="1"
 *                     ),
 *                     @OA\Property(
 *                         property="tipo",
 *                         type="string",
 *                         example="Suite"
 *                     ),
 *                     @OA\Property(
 *                         property="precio",
 *                         type="number",
 *                         format="float",
 *                         example=150.00
 *                     ),
 *                     @OA\Property(
 *                         property="created_at",
 *                         type="string",
 *                         example="2023-02-23T00:09:16.000000Z"
 *                     ),
 *                     @OA\Property(
 *                         property="updated_at",
 *                         type="string",
 *                         example="2023-02-23T12:33:45.000000Z"
 *                     )
 *                 )
 *             )
 *         )
 *     )
 * )
 */
    public function index()
    {
        $habitaciones = Habitacion::all();
        return response()->json($habitaciones);
    }

    /**
     * Muestra el formulario para crear una nueva habitación.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('habitaciones.create');
    }

    /**
 * Crea una nueva Habitación
 * @OA\Post(
 *     path="/api/habitaciones",
 *     tags={"Habitacion"},
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                      property="numero",
 *                      type="string"
 *                 ),
 *                 @OA\Property(
 *                      property="tipo",
 *                      type="string"
 *                 ),
 *                 @OA\Property(
 *                      property="precioNoche",
 *                      type="number",
 *                      format="float"
 *                 ),
 *                 @OA\Property(
 *                      property="disponibilidad",
 *                      type="boolean"
 *                 ),
 *                 @OA\Property(
 *                      property="capacidad",
 *                      type="integer",
 *                      format="int32"
 *                 ),
 *                 required={"numero", "tipo", "precioNoche", "disponibilidad", "capacidad"}, 
 * 
 *                 example={
 *                     "numero": "101",
 *                     "tipo": "Suite",
 *                     "precioNoche": 150.00,
 *                     "disponibilidad": true,
 *                     "capacidad": 2
 *                }
 *             )
 *         )
 *      ),
 *      @OA\Response(
 *          response=201,
 *          description="CREATED",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Habitación creada correctamente."),
 *          )
 *      ),
 *      @OA\Response(
 *          response=422,
 *          description="UNPROCESSABLE CONTENT",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="The numero field is required."),
 *              @OA\Property(property="errors", type="string", example="Objeto de errores"),
 *          )
 *      )
 * )
 */
public function store(Request $request)
{
    $request->validate([
        'numero' => 'required',
        'tipo' => 'required',
        'precioNoche' => 'required|numeric',
        'disponibilidad' => 'required|boolean',
        'capacidad' => 'required|integer',
        
    ]);

    Habitacion::create($request->all());

    return response()->json(['message' => 'Habitación creada correctamente.'], 201);
}


    /**
 * Muestra la habitación especificada.
 * @OA\Get(
 *     path="/api/habitaciones/{id}",
 *     tags={"Habitacion"},
 *     @OA\Parameter(
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="OK",
 *         @OA\JsonContent(
 *              @OA\Property(property="id", type="integer", example=1),
 *              @OA\Property(property="numero", type="string", example="101"),
 *              @OA\Property(property="tipo", type="string", example="Suite"),
 *              @OA\Property(property="precioNoche", type="number", format="float", example=150.00),
 *              @OA\Property(property="disponibilidad", type="boolean", example=true),
 *              @OA\Property(property="capacidad", type="integer", format="int32", example=2),
 *              @OA\Property(property="created_at", type="string", example="2023-02-23T00:09:16.000000Z"),
 *              @OA\Property(property="updated_at", type="string", example="2023-02-23T12:33:45.000000Z")
 *         )
 *     ),
 *      @OA\Response(
 *          response=404,
 *          description="NOT FOUND",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="No se encontró la habitación con el ID especificado."),
 *          )
 *      )
 * )
 */
public function show($id)
{
    $habitacion = Habitacion::findOrFail($id);
    return response()->json($habitacion);
}


    /**
     * Muestra el formulario para editar una habitación.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $habitacion = Habitacion::findOrFail($id);
        return view('habitaciones.edit', compact('habitacion'));
    }

  /**
 * Actualiza la habitación especificada en la base de datos.
 * @OA\Put(
 *     path="/api/habitaciones/{id}",
 *     tags={"Habitacion"},
 *     @OA\Parameter(
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                  @OA\Property(
 *                      property="numero",
 *                      type="string"
 *                 ),
 *                 @OA\Property(
 *                      property="tipo",
 *                      type="string"
 *                 ),
 *                 @OA\Property(
 *                      property="precioNoche",
 *                      type="number",
 *                      format="float"
 *                 ),
 *                 @OA\Property(
 *                      property="disponibilidad",
 *                      type="boolean"
 *                 ),
 *                 @OA\Property(
 *                      property="capacidad",
 *                      type="integer",
 *                      format="int32"
 *                 ),
 *                 required={"numero", "tipo", "precioNoche", "disponibilidad", "capacidad"}, 
 *                 example={
 *                     "numero": "101",
 *                     "tipo": "Suite",
 *                     "precioNoche": 150.00,
 *                     "disponibilidad": true,
 *                     "capacidad": 2
 *                }
 *             )
 *         )
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="OK",
 *          @OA\JsonContent(
 *              @OA\Property(property="id", type="integer", example=1),
 *              @OA\Property(property="numero", type="string", example="101"),
 *              @OA\Property(property="tipo", type="string", example="Suite"),
 *              @OA\Property(property="precioNoche", type="number", format="float", example=150.00),
 *              @OA\Property(property="disponibilidad", type="boolean", example=true),
 *              @OA\Property(property="capacidad", type="integer", format="int32", example=2),
 *              @OA\Property(property="created_at", type="string", example="2023-02-23T00:09:16.000000Z"),
 *              @OA\Property(property="updated_at", type="string", example="2023-02-23T12:33:45.000000Z")
 *          )
 *      ),
 *      @OA\Response(
 *          response=404,
 *          description="NOT FOUND",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="No se encontró la habitación con el ID especificado."),
 *          )
 *      ),
 *      @OA\Response(
 *          response=422,
 *          description="UNPROCESSABLE CONTENT",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="El tipo y el precio son campos obligatorios."),
 *              @OA\Property(property="errors", type="string", example="Objeto de errores"),
 *          )
 *      )
 * )
 */
public function update(Request $request, $id)
{
    $request->validate([
        'numero' => 'required',
        'tipo' => 'required',
        'precioNoche' => 'required|numeric',
        'disponibilidad' => 'required|boolean',
        'capacidad' => 'required|integer',
    ]);

    $habitacion = Habitacion::findOrFail($id);
    $habitacion->update($request->all());

    return response()->json($habitacion);
}


    /**
 * Elimina la habitación especificada de la base de datos.
 * @OA\Delete(
 *     path="/api/habitaciones/{id}",
 *     tags={"Habitacion"},
 *     @OA\Parameter(
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\Response(
 *         response=204,
 *         description="NO CONTENT",
 *         @OA\JsonContent(
 *             @OA\Property(property="message", type="string", example="Habitación eliminada correctamente.")
 *         )
 *     ),
 *      @OA\Response(
 *          response=404,
 *          description="NOT FOUND",
 *          @OA\JsonContent(
 *              @OA\Property(property="message", type="string", example="Habitación no encontrada.")
 *          )
 *      )
 * )
 */
public function destroy($id)
{
    $habitacion = Habitacion::find($id);

    if (!$habitacion) {
        return response()->json(['message' => 'Habitación no encontrada.'], 404);
    }

    $habitacion->delete();

    return response()->json(['message' => 'Habitación eliminada correctamente.'], 204);
}
}

```

## Conclusiones
En el desarrollo de este proyecto de gestión de reservas de habitaciones hemos logrado diseñar, implementar y desplegar un sistema que permite a nuestro cliente optimizar la administración de sus reservas y ofrecer una experiencia satisfactoria a sus huéspedes.

Mediante la utilización de tecnologías modernas y metodologías ágiles, se cumplio con los objetivos establecidos, brindando una solución robusta, escalable y fácil de usar, también ha contribuido a mejorar la experiencia general de los clientes, facilitando el proceso de reserva, el acceso a la información y la comunicación con el personal del hotel.

## Bibliografia
-  Laravel 8 Overview and Introduction to Jetstream
- https://youtu.be/VnIKwzysvHM?si=MD5tX5XDHv0pmm8Z
- https://youtu.be/kUCT88EJuCU?si=LAkCeFwIcL1im-me
## Anexos
### Marca de la Empresa
![imagen](https://live.staticflickr.com/65535/53558611181_4b7e30a47a_m.jpg)
### Nuestra Ubicación
[ubicacion](https://maps.app.goo.gl/qAaP5U2t8aLwnomr8)