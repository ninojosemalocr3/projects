# proyecto formativo: Modelado Backend de un sistema de gestion documental.
- Juan Carlos Sandoval Justiniano
- [Facebook](https://www.facebook.com/juanccsj/)
- [Whatsapp](https://wa.me/59165007148)

##	Introducción
Este sistema será creado para administrar la gestión documental de una empresa, abarcando todos los estándares de Almacenes, Clasificación Documental y los servicios para su mantenimiento permanente tales como ser Entrada de Documentos, Prestamos a los usuarios, Mantenimiento de Archivo y Salida Definitiva.


##	Objetivos
El Objetivo principal es desarrollar un Backend muy completo con la posibilidad de escalar y migrar a cualquier plataforma, que deberá facilitar la gestión documental de la empresa y todos los servicios que requiera.


##	Marco Teórico
###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```

###	MVC
El Modelo Vista Controlador (MVC) es un patrón arquitectónico utilizado en el desarrollo de aplicaciones de software que separa la representación de la información de la forma en que ésta se maneja y se presenta al usuario. Consiste en tres componentes principales:

1. **Modelo (Model):** Es la representación de los datos y la lógica de negocio de la aplicación. Se encarga de gestionar los datos, su lógica de negocio asociada y las reglas de acceso a dichos datos. En el contexto de una aplicación web para la gestión documental de una empresa, el modelo podría incluir clases que representan los diferentes tipos de documentos, métodos para acceder y manipular estos documentos en la base de datos, y cualquier otra lógica de negocio relacionada con la gestión de documentos.

2. **Vista (View):** Es la representación visual de los datos del modelo. Se encarga de mostrar la información al usuario de manera que sea comprensible y permita interactuar con ella. En una aplicación web para la gestión documental, las vistas podrían ser las páginas web que muestran la lista de documentos, los detalles de un documento específico, formularios para subir nuevos documentos, etc.

3. **Controlador (Controller):** Es el intermediario entre el modelo y la vista. Se encarga de interpretar las acciones del usuario, realizar las operaciones correspondientes en el modelo y actualizar la vista en consecuencia. En el caso de una aplicación web para la gestión documental, el controlador podría manejar las solicitudes HTTP recibidas desde el navegador del usuario, llamar a los métodos apropiados en el modelo para realizar operaciones como agregar, eliminar o editar documentos, y luego renderizar la vista correspondiente para mostrar los resultados al usuario.

###	Swagger para laravel
Swagger es un conjunto de herramientas de código abierto creadas en torno a la especificación de OpenAPI que nos pueden ayudar a diseñar, crear, documentar y consumir APIs REST de una manera sencilla.

## Prepacación del Proyecto
Para crear un ejemplo donde implementar Swagger vamos a empezar creando un nuevo proyecto de Laravel, usando la versión 5.8.
```bash
composer create-project --prefer-dist laravel/laravel swagger "5.8.*"
```

## Instalación de Swagger
Una vez creado nuestro proyecto, vamos a instalar el paquete darkaonline/l5-swagger:
```bash
composer require "darkaonline/l5-swagger:5.8.*"
```
##	Metodología
Se describe detalladamente la metodología utilizada en el proyecto, incluyendo los pasos específicos seguidos para llevar a cabo la investigación o desarrollo.
Através de la guía del docente desarrollo el proyecto comenzando por el modelado de la empresa y el diagrama entidad relación dela Base de Datos, fué necesario programar el proyecto y levantarlo con laravel que permite la integración en un solo lugar virtual que da la posibilidad de comunicar con el equipo anfitrión. Se programó el modelo de la base de datos en php para su posterior migración y la implementación de una API para ser utilizada a futuro y ofrece la posibilidad de ser compatible con la vista que le quisieramos dar más adelante.

### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53557688472_acf170dd22_b.jpg)
### Diagrama entidad relacion.
![diagrama](https://live.staticflickr.com/65535/53567007471_eb69bbdbb1_h.jpg)
### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        //tabla Ubicacion
        Schema::create('Ubicacion', function (Blueprint $table) {
            $table->id();
            $table->string('mueble')->nullable();                        
            $table->string('lado')->nullable();                        
            $table->string('nivel')->nullable();
            $table->string('sector')->nullable();
            $table->string('ubi_topografica')->nullable();
            $table->string('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Gerencia
        Schema::create('Gerencia', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('sigla')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Responsable
        Schema::create('Responsable', function (Blueprint $table) {
            $table->id();
            $table->string('cod_responsable')->nullable();                        
            $table->string('password')->nullable();                        
            $table->string('cargo')->nullable();
            $table->string('fecha_creacion')->nullable();
            $table->string('nombre')->nullable();
            $table->string('ap_paterno')->nullable();
            $table->string('ap_materno')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Solicitante
        Schema::create('Solicitante', function (Blueprint $table) {
            $table->id();
            $table->string('email')->nullable();                        
            $table->string('nombre')->nullable();
            $table->string('ap_paterno')->nullable();
            $table->string('ap_materno')->nullable();
            $table->unsignedBigInteger('gerencia');
            $table->foreign('gerencia')->references('id')->on('Gerencia');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Tipo_Servicio
        Schema::create('Tipo_Servicio', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Tipo_Documento
        Schema::create('Tipo_Documento', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();                        
            $table->string('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Caja
        Schema::create('Caja', function (Blueprint $table) {
            $table->id();
            $table->string('cod_caja')->nullable();                        
            $table->string('tipo_caja')->nullable();                        
            $table->string('estado')->nullable();
            $table->unsignedBigInteger('ubicacion');
            $table->foreign('ubicacion')->references('id')->on('Ubicacion');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Contenedor
        Schema::create('Contenedor', function (Blueprint $table) {
            $table->id();
            $table->string('cod_contenedor')->nullable();                        
            $table->string('volumen')->nullable();                        
            $table->string('gestion')->nullable();
            $table->string('prestado')->nullable();
            $table->unsignedBigInteger('caja');
            $table->foreign('caja')->references('id')->on('Caja');
            $table->unsignedBigInteger('gerencia');
            $table->foreign('gerencia')->references('id')->on('Gerencia');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Documento
        Schema::create('Documento', function (Blueprint $table) {
            $table->id();
            $table->string('cod_documento')->nullable();                        
            $table->date('fecha')->nullable();                        
            $table->string('responsable')->nullable();
            $table->unsignedBigInteger('contenedor');
            $table->foreign('contenedor')->references('id')->on('Contenedor');
            $table->unsignedBigInteger('tipo_documento');
            $table->foreign('tipo_documento')->references('id')->on('Tipo_Documento');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Atenciones
        Schema::create('Atenciones', function (Blueprint $table) {
            $table->id();
            $table->string('cod_atencion')->nullable();                        
            $table->date('fecha')->nullable();                        
            $table->string('descripcion')->nullable();
            $table->string('observacion')->nullable();
            $table->string('estado')->nullable();
            $table->unsignedBigInteger('solicitante');
            $table->foreign('solicitante')->references('id')->on('Solicitante');
            $table->unsignedBigInteger('servicio');
            $table->foreign('servicio')->references('id')->on('Tipo_Servicio');
            $table->unsignedBigInteger('responsable');
            $table->foreign('responsable')->references('id')->on('Responsable');
            $table->unsignedBigInteger('gerencia');
            $table->foreign('gerencia')->references('id')->on('Gerencia');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Detalle_Atenciones
        Schema::create('Detalle_Atenciones', function (Blueprint $table) {
            $table->id();
            $table->string('cod_documento')->nullable();                        
            $table->string('observacion')->nullable();
            $table->unsignedBigInteger('atencion');
            $table->foreign('atencion')->references('id')->on('Atenciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}

```
### explicacion de un caso de modelado.
El modelado se clasifica en 2 partes, una para gestionar el Almacen y su inventario, partiendo de que cada documento se almacena en un Contenedor o carpeta, éstos están almacenador en Cajas y éstas últimas tendrán una ubicación física, de esta manera facilitará el control del inventario y la ubicación eficiente de los documentos.
El otro sector del Modelado se centra en gestionar los servicios que se podrán ofrecer en el Archivo y la integración con los demás participantes, como los usuarios que administran el Archivo y los funcionarios de la empresa que harán las solicitudes al Archivo, etc.

### Datos seeder
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Tipo_Servicio;
use App\Models\Ubicacion;
use App\Models\Tipo_Documento;
use App\Models\Gerencia;
use App\Models\Responsable;
use App\Models\Solicitante;
use App\Models\Caja;
use App\Models\Contenedor;
use App\Models\Documento;
use App\Models\Atenciones;
use App\Models\Detalle_Atenciones;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipo_Servicio::create(['nombre'=>'Entrada Nueva','descripcion'=>'Entrada de documentos nuevos']);
        Tipo_Servicio::create(['nombre'=>'Prestamo','descripcion'=>'Prestamo de documentos fisicos']);
        Tipo_Servicio::create(['nombre'=>'Devolucion','descripcion'=>'Devolucion en fisico del documento']);
        Tipo_Servicio::create(['nombre'=>'Duplicacion','descripcion'=>'Copia digital de un documento']);
        Tipo_Servicio::create(['nombre'=>'Mantenimiento de Archivo','descripcion'=>'Algun servicio extra que requiera el documento']);
        Tipo_Servicio::create(['nombre'=>'Salida Definitiva','descripcion'=>'Documentos dado de baja']);

        Gerencia::create(['nombre'=>'Economico Administrativo','sigla'=>'ECOA']);
        Gerencia::create(['nombre'=>'Recursos Humanos','sigla'=>'RRHH']);
        Gerencia::create(['nombre'=>'Servicios Generales','sigla'=>'SEGE']);
        Gerencia::create(['nombre'=>'Administración','sigla'=>'ADM']);
        Gerencia::create(['nombre'=>'Comercial','sigla'=>'COM']);

        Responsable::create([
            'cod_responsable'=>'eriosl',
            'password'=>'2er82r71ntr',
            'cargo'=>'auxiliar1',
            'fecha_creacion'=>'2024-01-02',
            'nombre'=>'Epifanio',
            'ap_paterno'=>'Rios',
            'ap_materno'=>'llanos',
        ]);
        Responsable::create([
            'cod_responsable'=>'mtamboj',
            'password'=>'71v7r1363s6',
            'cargo'=>'auxiliar2',
            'fecha_creacion'=>'2024-01-03',
            'nombre'=>'Maicol',
            'ap_paterno'=>'Tambo',
            'ap_materno'=>'Justiniano',
        ]);
        Responsable::create([
            'cod_responsable'=>'jsandovalj',
            'password'=>'3s54da54f6540',
            'cargo'=>'admin',
            'fecha_creacion'=>'2024-01-01',
            'nombre'=>'Juan Carlos',
            'ap_paterno'=>'Sandoval',
            'ap_materno'=>'Justiniano',
        ]);
        Responsable::create([
            'cod_responsable'=>'rgutierrezp',
            'password'=>'9s8a7dv2c2s4',
            'cargo'=>'gestor de cuenta',
            'fecha_creacion'=>'2024-01-04',
            'nombre'=>'Roger',
            'ap_paterno'=>'Gutierrez',
            'ap_materno'=>'Parada',
        ]);
        Responsable::create([
            'cod_responsable'=>'lalmanzal',
            'password'=>'32a1s89a7sd564vr5',
            'cargo'=>'admin',
            'fecha_creacion'=>'2024-01-05',
            'nombre'=>'Luis',
            'ap_paterno'=>'Almanza',
            'ap_materno'=>'Lago',
        ]);

        Solicitante::create([
            'email'=>'esalazarm',
            'nombre'=>'Enrrique',
            'ap_paterno'=>'Salazar',
            'ap_materno'=>'Mendieta',
            'gerencia'=>1,
        ]);
        Solicitante::create([
            'email'=>'cdazar',
            'nombre'=>'Carlos',
            'ap_paterno'=>'Daza',
            'ap_materno'=>'Rios',
            'gerencia'=>2,
        ]);
        Solicitante::create([
            'email'=>'fchavezp',
            'nombre'=>'Federico',
            'ap_paterno'=>'Chavez',
            'ap_materno'=>'Prado',
            'gerencia'=>3,
        ]);
        Solicitante::create([
            'email'=>'areyesg',
            'nombre'=>'Alfonzo',
            'ap_paterno'=>'Reyes',
            'ap_materno'=>'Galindo',
            'gerencia'=>4,
        ]);
        Solicitante::create([
            'email'=>'mmarcelom',
            'nombre'=>'Marcelo',
            'ap_paterno'=>'Martins',
            'ap_materno'=>'Moreno',
            'gerencia'=>5,
        ]);
        
        Tipo_Documento::create(['nombre'=>'correspondencia',]);
        Tipo_Documento::create(['nombre'=>'file de personal',]);
        Tipo_Documento::create(['nombre'=>'asiento contable',]);
        Tipo_Documento::create(['nombre'=>'finiquito',]);
        Tipo_Documento::create(['nombre'=>'orden de compra',]);

        Ubicacion::create([
            'mueble'=>'1',
            'lado'=>'A',
            'nivel'=>'1',
            'sector'=>'1',
            'ubi_topografica'=>'M1-LA-N1-S1',
        ]);
        Ubicacion::create([
            'mueble'=>'1',
            'lado'=>'A',
            'nivel'=>'1',
            'sector'=>'2',
            'ubi_topografica'=>'M1-LA-N1-S2',
        ]);

        Caja::create([
            'cod_caja'=>'B-0001',
            'tipo_caja'=>'carton',
            'estado'=>'abierta',
            'ubicacion'=>1,
        ]);
        Caja::create([
            'cod_caja'=>'B-0002',
            'tipo_caja'=>'carton',
            'estado'=>'cerrada',
            'ubicacion'=>2,
        ]);

        Contenedor::create([
            'cod_contenedor'=>'C-0001',
            'volumen'=>'3',
            'gestion'=>'2000',
            'prestado'=>'0',
            'caja'=>1,
            'gerencia'=>1,
        ]);
        Contenedor::create([
            'cod_contenedor'=>'C-0002',
            'volumen'=>'2',
            'gestion'=>'2005',
            'prestado'=>'0',
            'caja'=>2,
            'gerencia'=>2,
        ]);

        Documento::create([
            'cod_documento'=>'D-0001',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento1',
            'prestado'=>'0',
            'contenedor'=>1,
            'tipo_documento'=>5,
        ]);
        Documento::create([
            'cod_documento'=>'D-0002',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento2',
            'prestado'=>'0',
            'contenedor'=>1,
            'tipo_documento'=>4,
        ]);
        Documento::create([
            'cod_documento'=>'D-0003',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento3',
            'prestado'=>'0',
            'contenedor'=>1,
            'tipo_documento'=>3,
        ]);
        Documento::create([
            'cod_documento'=>'D-0004',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento4',
            'prestado'=>'0',
            'contenedor'=>1,
            'tipo_documento'=>2,
        ]);
        Documento::create([
            'cod_documento'=>'D-0005',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento5',
            'prestado'=>'0',
            'contenedor'=>1,
            'tipo_documento'=>1,
        ]);
        Documento::create([
            'cod_documento'=>'D-0006',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento6',
            'prestado'=>'0',
            'contenedor'=>2,
            'tipo_documento'=>5,
        ]);
        Documento::create([
            'cod_documento'=>'D-0007',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento7',
            'prestado'=>'0',
            'contenedor'=>2,
            'tipo_documento'=>4,
        ]);
        Documento::create([
            'cod_documento'=>'D-0008',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento8',
            'prestado'=>'0',
            'contenedor'=>2,
            'tipo_documento'=>3,
        ]);
        Documento::create([
            'cod_documento'=>'D-0009',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento9',
            'prestado'=>'0',
            'contenedor'=>2,
            'tipo_documento'=>2,
        ]);
        Documento::create([
            'cod_documento'=>'D-0010',
            'fecha'=>'2024-03-03',
            'descripcion'=>'documento10',
            'prestado'=>'0',
            'contenedor'=>2,
            'tipo_documento'=>1,
        ]);

        Atencion::create([
            'cod_atencion'=>'AT-0001',
            'fecha'=>'2024-03-03',
            'descripcion'=>'atencion1',
            'estado'=>'cerrada',
            'solicitante'=>1,
            'servicio'=>1,
            'responsable'=>2,
            'gerencia'=>1,
        ]);
        Atencion::create([
            'cod_atencion'=>'AT-0002',
            'fecha'=>'2024-03-04',
            'descripcion'=>'atencion2',
            'estado'=>'cerrada',
            'solicitante'=>3,
            'servicio'=>2,
            'responsable'=>3,
            'gerencia'=>2,
        ]);
        Atencion::create([
            'cod_atencion'=>'AT-0003',
            'fecha'=>'2024-03-05',
            'descripcion'=>'atencion3',
            'estado'=>'cerrada',
            'solicitante'=>3,
            'servicio'=>3,
            'responsable'=>5,
            'gerencia'=>2,
        ]);
        Atencion::create([
            'cod_atencion'=>'AT-0004',
            'fecha'=>'2024-03-06',
            'descripcion'=>'atencion4',
            'estado'=>'cerrada',
            'solicitante'=>4,
            'servicio'=>4,
            'responsable'=>5,
            'gerencia'=>1,
        ]);

        Detalle_Atenciones::create(['cod_documento'=>'D-0001','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0002','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0003','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0004','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0005','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0006','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0007','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0008','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0009','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0010','atencion'=>1,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0002','atencion'=>2,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0003','atencion'=>2,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0004','atencion'=>2,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0002','atencion'=>3,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0003','atencion'=>3,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0004','atencion'=>3,]);
        Detalle_Atenciones::create(['cod_documento'=>'D-0006','atencion'=>4,]);
    }
}
```

### APi.
Se desarrollo las APIs necesarias para toda la Base de datos en php, desde consultas para mostrar los datos de una sola tabla y otras que involucran más de una tabla obteniendo los datos foráneos.
```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Tipo_Servicio;
use App\Models\Ubicacion;
use App\Models\Tipo_Documento;
use App\Models\Gerencia;
use App\Models\Responsable;
use App\Models\Solicitante;
use App\Models\Caja;
use App\Models\Contenedor;
use App\Models\Documento;
use App\Models\Atenciones;
use App\Models\Detalle_Atenciones;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('tipo_servicio',function(){
    return Tipo_Servicio::all();
});

Route::get('ubicacion',function(){
    return Ubicacion::all();
});

Route::get('tipo_documento',function(){
    return Tipo_Documento::all();
});

Route::get('gerencia',function(){
    return Gerencia::all();
});

Route::get('responsable',function(){
    return Responsable::all();
});

Route::get('solicitante',function(){
    return Solicitante::all();
});

Route::get('caja', function() {
    return Caja::with('ubicacion:id,ubi_topografica')->get();
});

Route::get('contenedor', function() {
    return Contenedor::with('caja:id,cod_caja', 'gerencia:id,nombre')->get();
});

Route::get('documento', function() {
    return Documento::with('contenedor:id,cod_contenedor', 'tipo_documento:id,nombre')->get();
});

Route::get('documento2', function() {
    return Documento::with('contenedor.caja.ubicacion')->get();
});

Route::get('atenciones', function() {
    return Atenciones::with('gerencia:id,nombre', 'responsable:id,cod_responsable', 'solicitante:id,nombre,ap_paterno', 'servicio:id,nombre')->get();
});

Route::get('detalle_atencion', function() {
    return Detalle_Atenciones::with('atencion:id,cod_atencion', 'cod_documento:id,cod_documento')->get();
});
```

##	Conclusiones
1. **Complejidad del Sistema:** El proyecto se centra en la creación de un backend completo para la gestión documental de una empresa, lo que implica una planificación y ejecución detallada de diversos aspectos como el modelado de datos, la implementación de funcionalidades y la creación de APIs.

2. **Uso de Tecnologías Modernas:** Se emplean tecnologías modernas como Laravel, un framework de PHP, que facilita el desarrollo rápido y eficiente de aplicaciones web. Además, se hace uso de herramientas como Swagger para documentar y consumir APIs REST de manera sencilla.

3. **Metodología de Desarrollo:** Se describe una metodología de desarrollo que incluye la planificación detallada de los pasos a seguir, desde el modelado de datos hasta la implementación de APIs, lo que sugiere un enfoque estructurado y organizado en el proceso de desarrollo.

4. **Modelado de Datos:** Se presenta un modelado detallado de la base de datos utilizando migraciones en Laravel, lo que permite una representación clara de la estructura de datos y las relaciones entre las diferentes entidades.

5. **APIs para Interacción:** Se desarrollan APIs para interactuar con los datos almacenados en la base de datos, lo que proporciona una forma estandarizada y eficiente de acceder y manipular la información desde distintas aplicaciones o servicios.

6. **Escalabilidad y Adaptabilidad:** Se destaca la posibilidad de escalar y adaptar el sistema a las necesidades específicas de cualquier empresa, lo que sugiere una arquitectura flexible y modular que puede ajustarse a diferentes contextos y requisitos.

##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234
-  Patterns of Enterprise Application Architecture (2002)  Martin Fowler
-  https://styde.net/como-documentar-una-api-en-laravel-usando-swagger/
### foto de la empresa para la cual estan desarrollando
No se ha desarrollado para una empresa en específico, sino un sistema genérico que puede adaptarse a las necesidades de cualquier empresa con la posibilidad de escalabilidad y adaptabilidad
