# VENTA DE ELECTRONICOS EN LINEA

### 1.- Readme del proyecto:

## DESCRIPCION

---

Nuestro Sistema de Ventas de Artículos Electrónicos es una plataforma avanzada diseñada para optimizar
y simplificar el proceso de compra y venta de productos electrónicos de última generación. Con una interfaz
intuitiva y funcionalidades robustas, nuestro sistema proporciona una experiencia sin igual tanto para los
vendedores como para los compradores.

### Integrantes del Grupo C#

---

- Diana Estefany Castano Rodas
- Juan Carlos Guerra Colque
- Jose Ignacio Burgos Ayala
- Luis Jose Baldelomar Tapia

![Image text](IMAGEN/Animation.gif)

### INSTALACION 
---
### Visual Studio 2022


- [Visual Studio 2022](https://estradawebgroup.com/Post/Como-instalar-Visual-Studio-2022/20523): Version 12.3

##

## 2.- Fundamentacion del sistema:
---
## ¿PORQUE VENTA DE ELECTRONICOS EN LINEA?

El sistemas de ventas en electronicas resuelven los inconvenientes que se les presenten al cliente al momento de realizar una compra de un producto ya que es un proceso diseñado para ser mas rentables los esfuerzos comerciales de una organizacion, tambien puede aumentar la productividad y maximizar sus ingresos

## PROBLEMAS
### LAS COLAS
Las colas o filas en una tienda física pueden afectar tanto a los clientes como al negocio de diversas maneras. 

#### Experiencia del cliente:

1. Insatisfacción: Esperar en largas colas puede generar frustración y una experiencia negativa para el cliente.
2. Abandono de compras: Si la espera es muy larga, algunos clientes pueden decidir abandonar sus compras y salir de la tienda.
3. Percepción de la marca: Una gestión ineficiente de las colas puede llevar a que los clientes tengan una percepción negativa de la marca o tienda.

#### Ventas y rentabilidad:

1. Reducción de ventas: Como mencionado, la posibilidad de que clientes abandonen sus compras puede reducir las ventas.
2. Pérdida de clientes recurrentes: Una mala experiencia puede hacer que algunos clientes decidan no volver a la tienda en el futuro.

## SOLUCION

Para evitar la pérdida de clientes y ventas, creamos un sitio web de nombre "VENTA DE ELECTRONICOS EN LINEA" donde el cliente tendrá disponibilidad 24/7 de nuestros productos a través de nuestro sitio web donde el cliente puede realizar sus pedidos sin hacer fila y de esta manera no perder clientes y evitar pérdidas en Ventas.


## IMPORTANCIA

---

Los sistemas de ventas electrónicas son herramientas esenciales en el comercio moderno, ya que permiten a las
empresas vender productos y servicios en línea de manera eficiente y segura. Estos sistemas agilizan las
transacciones, amplían el alcance a una audiencia global y brindan comodidad a los clientes al facilitar
la compra desde cualquier lugar y en cualquier momento. Además, ayudan a automatizar procesos de facturación y
seguimiento de inventario, lo que optimiza la gestión empresarial. Su importancia radica en impulsar el crecimiento
del negocio y mejorar la experiencia del cliente en el entorno digital.

## Imagenes de Duolingo

###

![Image text](IMAGEN/duolingo.JPG)
![Image text](IMAGEN/carlos.jpeg)
###

## 3.- Trabajo colaborativo:

- [Grupo C#](https://drive.google.com/file/d/11pl-6rZ6vnXpJ9SF711mX7IcxzjT6UDb/view?usp=sharing): evidencia

## 4.- Distribucion de Roles:

###

cada integrante del grupo aporto con la informacion dada de los puntos correspondientes como tambien hicimos una investigacion por cuenta propia

###
