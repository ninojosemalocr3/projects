Vue.createApp({
    data() {
        return {
            nombre: '',
            apellido: '',
            correo: '',
            nombreRegistrado: '',
            apellidoRegistrado: '',
            correoRegistrado: '',
            MostrarFormulario:true,
        };
    },
    methods: {
        enviarFormulario() {
            this.MostrarFormulario=false;
            this.nombreRegistrado = this.nombre;
            this.apellidoRegistrado = this.apellido;
            this.correoRegistrado = this.correo;
        },
        NuevoUsuario(){
            this.MostrarFormulario=true;
            this.nombre= '';
            this.apellido= '';
            this.correo= '';
        }
    }
}).mount('#forUsuario');